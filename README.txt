This source code enhances the LIBSVM training process' performance through OpenCL parallelization. It also improves dataset representation, requiring less memory to represent datasets.

Currently, it only works with the datasets provided with the source code at folder source/datasets, although it is expected to increase coverage in future versions.

The parameters of the generated .exe file are the same as the original LIBSVM, requiring only a .train file (provided by LIBSVM: http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/) as input and a .model file as output.

Any problems report to henry@inf.ufsm.br.