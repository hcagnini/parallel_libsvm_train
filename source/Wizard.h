#ifndef __WIZARD__H__
#define __WIZARD__H__

#include <CL\cl.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "psvm.h"

#define USE_CPU 0
#define USE_GPU 1

using namespace std;

class Wizard {

private:

unsigned int gcd(unsigned int x, unsigned int y) {
    while (x * y != 0) {
        (x >= y)? x = x % y : y = y % x;
    }
    return (x + y);
}

protected:

	cl_platform_id platform;
	cl_device_id *devices;
	cl_context context;
	cl_command_queue commandQueue;
	cl_kernel kernel;
	cl_program program;
	
	//wait event list
	int wait_list_size;
	cl_event *wait_list;

	//memory itens
	cl_mem mem_out, mem_y, mem_x_square, mem_str_len, mem_node_full, mem_gamma;

	//convert the kernel file into a string
	std::string convertKernelToString(const char *filename) {
		size_t size;
		char*  str;
		std::fstream f(filename, (std::fstream::in | std::fstream::binary));

		if (f.is_open()) {
			size_t fileSize;
			f.seekg(0, std::fstream::end);
			size = fileSize = (size_t)f.tellg();
			f.seekg(0, std::fstream::beg);
			str = new char[size + 1];
			/*
			if (!str) { 
				f.close();
				return;
			}*/
			f.read(str, fileSize);
			f.close();
			str[size] = '\0';
			return str;
		}
		std::string error = std::string("Error: failed to open file ") + std::string(filename);
		throw exception(error.c_str());
	}

	//return the OpenCL error strings, given an OpenCL error code.
	const inline char *getErrorMessage(cl_int err) {
		switch (err) {
			case 0: return "CL_SUCCESS";
			case -1: return "CL_DEVICE_NOT_FOUND";
			case -2: return "CL_DEVICE_NOT_AVAILABLE";
			case -3: return "CL_COMPILER_NOT_AVAILABLE";
			case -4: return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
			case -5: return "CL_OUT_OF_RESOURCES";
			case -6: return "CL_OUT_OF_HOST_MEMORY";
			case -7: return "CL_PROFILING_INFO_NOT_AVAILABLE";
			case -8: return "CL_MEM_COPY_OVERLAP";
			case -9: return "CL_IMAGE_FORMAT_MISMATCH";
			case -10: return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
			case -11: return "CL_BUILD_PROGRAM_FAILURE";
			case -12: return "CL_MAP_FAILURE";
			case -30: return "CL_INVALID_VALUE";
			case -31: return "CL_INVALID_DEVICE_TYPE";
			case -32: return "CL_INVALID_PLATFORM";
			case -33: return "CL_INVALID_DEVICE";
			case -34: return "CL_INVALID_CONTEXT";
			case -35: return "CL_INVALID_QUEUE_PROPERTIES";
			case -36: return "CL_INVALID_COMMAND_QUEUE";
			case -37: return "CL_INVALID_HOST_PTR";
			case -38: return "CL_INVALID_MEM_OBJECT";
			case -39: return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
			case -40: return "CL_INVALID_IMAGE_SIZE";
			case -41: return "CL_INVALID_SAMPLER";
			case -42: return "CL_INVALID_BINARY";
			case -43: return "CL_INVALID_BUILD_OPTIONS";
			case -44: return "CL_INVALID_PROGRAM";
			case -45: return "CL_INVALID_PROGRAM_EXECUTABLE";
			case -46: return "CL_INVALID_KERNEL_NAME";
			case -47: return "CL_INVALID_KERNEL_DEFINITION";
			case -48: return "CL_INVALID_KERNEL";
			case -49: return "CL_INVALID_ARG_INDEX";
			case -50: return "CL_INVALID_ARG_VALUE";
			case -51: return "CL_INVALID_ARG_SIZE";
			case -52: return "CL_INVALID_KERNEL_ARGS";
			case -53: return "CL_INVALID_WORK_DIMENSION";
			case -54: return "CL_INVALID_WORK_GROUP_SIZE";
			case -55: return "CL_INVALID_WORK_ITEM_SIZE";
			case -56: return "CL_INVALID_GLOBAL_OFFSET";
			case -57: return "CL_INVALID_EVENT_WAIT_LIST";
			case -58: return "CL_INVALID_EVENT";
			case -59: return "CL_INVALID_OPERATION";
			case -60: return "CL_INVALID_GL_OBJECT";
			case -61: return "CL_INVALID_BUFFER_SIZE";
			case -62: return "CL_INVALID_MIP_LEVEL";
			case -63: return "CL_INVALID_GLOBAL_WORK_SIZE";
			default: return "Unknown OpenCL error";
		}
	}

	inline void handleErrorMessage(cl_int err) {
		if (err != CL_SUCCESS) {
			cout << getErrorMessage(err) << endl;
			throw exception(getErrorMessage(err));
		}
	}

	void buildProgram(const char *kernel_filename) {
		program = createProgram(kernel_filename);
		cl_int err = clBuildProgram(program, 1, devices, NULL, NULL, NULL);

		if(err != CL_SUCCESS) {
			size_t length;
			char buffer[8192]; //size OpenCL debug returning string
			cl_int err0 = clGetProgramBuildInfo(program, devices[0], CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &length);
			handleErrorMessage(err0);
			std::cout << "--- Build log ---" << std::endl << buffer << std::endl;
		}
		handleErrorMessage(err);
	}

	cl_uint getNumberOfPlatforms() {
		cl_uint numPlatforms;	//the NO. of platforms
		cl_int	err = clGetPlatformIDs(0, NULL, &numPlatforms);
		handleErrorMessage(err);
		return numPlatforms;
	}

	/**
	 * Picks the first avaiable platform.
	 * Documenta��o: "The host plus a collection 
	 * of devices managed by the OpenCL framework 
	 * that allow an application to share resources 
	 * and execute kernels on devices in the platform." 
	 */
	void getPlatform() {
		cl_uint numberPlatforms = getNumberOfPlatforms();
		if (numberPlatforms > 0) {
			cl_platform_id* platforms = (cl_platform_id*)malloc(numberPlatforms * sizeof(cl_platform_id));
			cl_int err = clGetPlatformIDs(numberPlatforms, platforms, NULL);
			handleErrorMessage(err);
			platform = platforms[0];
			free(platforms);

			//write platform info.
			char answer[1024];
			clGetPlatformInfo(platform, CL_PLATFORM_VERSION, 1024, (void*)&answer, NULL);
			cout << answer << endl;

		} else {
			throw exception("No avaiable platform!");
		}
	}

	/**
	 * Picks the first avaiable GPU, or OpenCL compatible CPU.
	 */
	void getDevice() {
		cl_int err;
		cl_uint	gpuDevices, cpuDevices;

		err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, &gpuDevices);
		err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, &cpuDevices);

		if (gpuDevices == 0) {
			cout << "Using CPU" << endl;
		
			devices = (cl_device_id*)malloc(cpuDevices * sizeof(cl_device_id));
			err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, cpuDevices, devices, NULL);
			handleErrorMessage(err);
		} else {
			cout << "Using GPU" << endl;
			devices = (cl_device_id*)malloc(gpuDevices * sizeof(cl_device_id));
			err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, gpuDevices, devices, NULL);
			handleErrorMessage(err);
		}	
	}

	void createContext() {
		cl_int err;
		context = clCreateContext(NULL, 1, devices, NULL, NULL, &err);
		handleErrorMessage(err);
	}

	void createCommandQueue() {
		cl_int err;
		commandQueue = clCreateCommandQueue(context, devices[0], 0, &err);
		handleErrorMessage(err);
	}

	/**
	 * Creates the program.
	 * filename - kernel filename
	 */
	cl_program createProgram(const char *filename) {
		string sourceStr = convertKernelToString(filename);
		
		cl_int err = CL_SUCCESS;
		const char *source = sourceStr.c_str();
		
		const cl_uint size = 1;
		size_t sourceSize[size] = { strlen(source) };

		cl_program program = clCreateProgramWithSource(context, size, &source, sourceSize, &err);
		handleErrorMessage(err);
		return program;
	}

public:

	/**
	* Wizard constructor.
	* Parameters:
	* kernel_filename - Name of kernel file.
	* program_name - Name of kernel function inside kernel file.
	*/
	Wizard(const char *kernel_filename, const char *program_name) {
		cl_int err;

		getPlatform();
		getDevice();

		createContext();
		createCommandQueue();
		buildProgram(kernel_filename);

		//cria kernel
		kernel = clCreateKernel(program, program_name, &err);
		handleErrorMessage(err);
	}

	~Wizard() {
		cl_int err = CL_SUCCESS;

		err = clReleaseKernel(kernel); //Release kernel.
		handleErrorMessage(err);
		err = clReleaseProgram(program); //Release the program object.
		handleErrorMessage(err);
		err = clReleaseCommandQueue(commandQueue); //Release  Command queue.
		handleErrorMessage(err);
		err = clReleaseContext(context); //Release context.
		handleErrorMessage(err);

		if (devices != NULL) {
			delete devices;
		}
	}

	void releaseEvents() {
		cl_int err = CL_SUCCESS;
		
		err = clWaitForEvents(wait_list_size, wait_list);
		handleErrorMessage(err);

		if(wait_list_size > 0) {
			for(int n = 0; n < wait_list_size; n++) {
				err = clReleaseEvent(wait_list[n]);
				handleErrorMessage(err);
			}
			wait_list_size = 0;
		}
	}

	void createClassBuffer(schar *y, int node_height) {
		cl_int err = CL_SUCCESS;

		mem_y = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, node_height * sizeof(schar), (void*)y, &err);
		handleErrorMessage(err);
	}

	void releaseClassBuffer() {
		cl_int err = clReleaseMemObject(mem_y);
		handleErrorMessage(err);
	}

	void releaseSvmKernelBuffers() {
		const int vec_size = 4;
		cl_mem vec[vec_size] = {mem_out, mem_x_square, mem_str_len, mem_node_full};

		cl_int err;
		for(int n = 0; n < vec_size; n++) {
			err = clReleaseMemObject(vec[n]);
			handleErrorMessage(err);
		}
	}

	void createSvmKernelBuffers(float gamma, float *x_square, const int node_height, const int str_len, unsigned char **node_full) {
		cl_int err = CL_SUCCESS;

		//cria itens na mem�ria
		mem_out = clCreateBuffer(context, CL_MEM_WRITE_ONLY, node_height * sizeof(Qfloat), NULL, &err);
		handleErrorMessage(err);

		mem_gamma = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float), (void*)&gamma, &err);
		handleErrorMessage(err);

		mem_x_square = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, node_height * sizeof(float), (void*)x_square, &err);
		handleErrorMessage(err);

		mem_str_len = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), (void*)&str_len, &err);
		handleErrorMessage(err);

		mem_node_full = clCreateBuffer(context, CL_MEM_READ_ONLY, node_height * str_len * sizeof(unsigned char), NULL, &err);
		handleErrorMessage(err);

		//writes node_full data into GPU memory
		size_t offset = 0;
		//cl_event current;

		//wait_list_size = node_height + 2; //+2 = execution and reading
		//wait_list = new cl_event [wait_list_size]; 

		for(int n = 0; n < node_height; n++) { 
			err = clEnqueueWriteBuffer(commandQueue, mem_node_full, CL_TRUE, offset, str_len * sizeof(unsigned char), (void*)node_full[n], 0, NULL, NULL);
			handleErrorMessage(err);

			offset += str_len * sizeof(unsigned char);
		}
	}

	void run_rbf_getQ(int choose, int start, int length, int work_item_per_work_group, Qfloat *data) {
		cl_int err = CL_SUCCESS;

		cl_mem mem_choose = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), (void*)&choose, &err);
		handleErrorMessage(err);

		cl_mem mem_start = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), (void*)&start, &err);
		handleErrorMessage(err);

		//seta argumentos do kernel
		const int param_size = 8;
		const cl_mem params[param_size] = {mem_out, mem_choose, mem_start, mem_gamma, mem_y, mem_x_square, mem_str_len, mem_node_full};

		for(int n = 0; n < param_size; n++) {
			err = clSetKernelArg(kernel, n, sizeof(cl_mem), (void*)&params[n]); 
			handleErrorMessage(err);
		}

		const int work_dim = 1;

		size_t 
			max_work_group_size,
			global_work_size[work_dim] = {length},
			local_work_size[work_dim] = {work_item_per_work_group};

		clGetDeviceInfo(devices[0], CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_work_group_size, NULL);

		if((size_t)work_item_per_work_group > max_work_group_size) {
			local_work_size[0] = (size_t)gcd(length, (int)max_work_group_size); //gcd = greatest common divisor
		} 

		cl_event event_run, event_read;
		
		err = clEnqueueNDRangeKernel(commandQueue, kernel, work_dim, NULL, global_work_size, local_work_size, 0, NULL, &event_run); 
		handleErrorMessage(err);
		
		//params[0] = mem_out
		err = clEnqueueReadBuffer(commandQueue, params[0], CL_TRUE, 0, length * sizeof(float), data, 1, &event_run, &event_read);
		handleErrorMessage(err);
		
		//waits reading
		clWaitForEvents(1, &event_read);

		//release data used only in this functiion call
		err = clReleaseMemObject(mem_choose);
		handleErrorMessage(err);

		err = clReleaseMemObject(mem_start);
		handleErrorMessage(err);
	}
};

#endif //__WIZARD__H__