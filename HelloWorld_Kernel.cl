typedef signed char schar;

typedef float Qfloat;

//if using OpenCL version below 1.2, use this function
/*
int popcount(unsigned char value) {
	int z, sum = 0;
	for(z = 128; z > 0; z >>= 1) {
		if((value & z) == z) {
			sum++;
		}
	}
	return sum;
}
//*/

__kernel void run_rbf_getQ(
							__global Qfloat *out,					//vector
							__global int *choose,					//escalar						
							__global int *start,					//escalar
							__global float *gamma,					//escalar
							__global schar *y,						//vector
							__global float *x_square,				//vector
							__global int *str_len,					//escalar
							__global unsigned char *node_full) {	//vector

	int
		n, 
		sum = 0,
		thread_index = get_global_id(0);
	
	__global unsigned char 
		*node_full_pivot = node_full + (*choose * *str_len),
		*node_full_change = node_full + ((thread_index + *start) * *str_len);

	__global schar 
		*y_pivot = y + *choose,
		*y_change = y + (thread_index + *start);

	__global float 
		*x_square_pivot = x_square + *choose,
		*x_square_change = x_square + (thread_index + *start);

	for(n = 0; n < (*str_len); n++) {
		sum += popcount(
			(*node_full_pivot) & (*node_full_change)
		);

		node_full_pivot++;
		node_full_change++;
	}
	
	out[thread_index] = 
		(*y_pivot) * (*y_change) * (
			native_exp(
				(-(*gamma)) * (
					(*x_square_pivot) + (*x_square_change) - 2 * sum
				)
			)
		);
}